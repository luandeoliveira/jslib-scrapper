const puppeteer = require('puppeteer');

const MATRICULA = '218120408';
const SENHA = '1512';


const getData = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('http://www.pergamum.bib.ufba.br/pergamum/mobile/login.php?flag=renovacao.php', {waitUntil: 'networkidle2'});
  await page.type('#id_login', MATRICULA);
  await page.type('#id_senhaLogin', SENHA);
  await page.click('#button');
  await page.waitForSelector('h2');  

  /* let titulos = await page.evaluate(el => el.innerHTML, await page.$('h2').toArray());
  let datas = await page.evaluate(el => el.innerHTML, await page.$('.ui-li-desc').toArray());
  let numeroRenovacoes = await page.evaluate(el => el.innerHTML, await page.$('.ui-li-desc').toArray());  
  let re = /(\d{2}\/\d{2}\/\d{4})/mg; 
  let datasfix = String(datas).match(re); */

  const dados = await page.evaluate(() => {

    //Extraindo os elementos da página 
    let titulos = $('h2').innerHTML.toArray();  
    let datasERenovacoes = $('.ui-li-desc').innerHTML.toArray();    

    // ----> Caso não dê certo, fazer um map com os componentes,  retornando apenas o innerHTML

    //Separando as datas do n° de renovações 
    let datas = datasERenovacoes.filter(value => String(value).includes('Data de devolução:'));
    let renovacoes = datasERenovacoes.filter(value => String(value).includes('N° de Renovações:'));
    
    //Pegando somente o valor desejado do innerHTML de cada elemento 
    let reData = /(\d{2}\/\d{2}\/\d{4})/mg; 
    datas = datas.map(value => values.match(reData))    
    let reRenovacao = /\d/mg;
    renovacoes = renovacoes.map(value => values.match(reRenovacao));

    return titulos.map((value, i) => {
      return {titulo: titulos[i], data: datas[i], renovacoes: [i]}   
    })
  })
   /*  PARA RESOLVER O PROBLEMA DE APERTAR O BOTAO: FAZER UM ARRAY COM OS 
    BOTOES E RELACIONAR COM O ÍNDICE DO OBJETO PRINCIPAL */

  await browser.close();  

  return dados;
  
  
};

(async () => {
  console.log(await getData())
})()




