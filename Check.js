const getData = require("./getData");
const puppeteer = require("puppeteer");
const moment = require("moment");

(async () => {
  const dados = await getData();
  await dados.datas.map(data => {
    let today = moment();
    let dia = data.format("DD");
    let mes = data.format("MM");

    if (dia == today.format("DD") && mes == today.format("MM")) {
      //Fazer ação para clicar em botão

      (async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto(
          "http://www.pergamum.bib.ufba.br/pergamum/mobile/login.php?flag=renovacao.php",
          { waitUntil: "networkidle2" }
        );
        await page.type("#id_login", "218120408");
        await page.type("#id_senhaLogin", "1512");
        await page.click("#button");

        const button = await page.$x(
          `//a[contains(h2, ${dados.titulos[dados.datas.indexOf(data)]})]`
        );
        await button.click();
        await page.waitForSelector(
          ".ui-btn ui-shadow ui-btn-corner-all ui-btn-icon-left ui-btn-up-a"
        );
        await page.click(
          "ui-btn ui-shadow ui-btn-corner-all ui-btn-icon-left ui-btn-up-a"
        );

        await browser.close();
      })();
    } else {
      console.log("Ainda não está no dia de renovar");
    }
  });
})();
