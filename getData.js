const puppeteer = require("puppeteer");
const moment = require("moment");

const getData = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(
    "http://www.pergamum.bib.ufba.br/pergamum/mobile/login.php?flag=renovacao.php",
    { waitUntil: "networkidle2" }
  );
  await page.type("#id_login", "218120408");
  await page.type("#id_senhaLogin", "");
  await page.click("#button");
  await page.waitForSelector("h2");

  let dados = await page.evaluate(() => {
    let titulos = [];
    let dr = [];

    let elementosTitulos = document.getElementsByTagName("h2");
    let elementosDr = document.getElementsByClassName("ui-li-desc");

    for (var element of elementosTitulos) titulos.push(element.textContent);

    for (var element of elementosDr) dr.push(element.textContent);

    return { titulos: titulos, datas: dr };
  });

  let titulos = await dados.titulos;
  let datas = await dados.datas.filter(value =>
    String(value).includes("Data de devolução:")
  );
  let renovacoes = await dados.datas.filter(value =>
    String(value).includes("N° de Renovações:")
  );

  let reData = /(\d{2}\/\d{2}\/\d{4})/gm;
  datas = datas.map(value => value.match(reData));
  datas = datas.map(value => String(value).replace(/\//, "-"));

  let reRenovacao = /\d/gm;
  renovacoes = renovacoes.map(value => value.match(reRenovacao));

  await browser.close();

  return {
    titulos: titulos,
    datas: datas.map(d => moment(d, "DD-MM/YYYY")),
    renovacoes: renovacoes
  };
};

/* (async () => {
    console.log(await getData())
  })() */

module.exports = getData;
